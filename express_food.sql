-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 19, 2018 at 10:01 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `express_food`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `street_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `house_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flat_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `street_name`, `house_number`, `flat_number`, `postcode`, `city`, `country`, `comment`) VALUES
(1, 'Cunnery Rd', '20', '3A', 'DD8 8JZ', 'Mains of Ballingard', 'United Kingdom', 'First floor, second door on the right'),
(2, 'Golden Knowes Road', '44', '12', 'CV23 4AA', 'Frankton', 'United Kingdom', 'Door bell is not functional, please call mobile phone when delivery arrives'),
(3, 'Telford Street', '24', '11', 'CV13 6BE', 'Barlestone', 'United Kingdom', 'Watch out for the dog, it bites'),
(4, 'St Maurices Road', '95', '2', 'GL52 5RZ', 'Prestbury', 'United Kingdom', 'Follow the small trail at Junction Street leading up the hill to find the house.'),
(5, 'Peachfield Road', '91', '1', 'PO38 6UD', 'Chale Green', 'United Kingdom', 'N/A'),
(6, 'Sloe Lane', '67', '11', 'G65 0AB', 'Croy', 'United Kingdom', 'I only have £20 notes.'),
(7, 'Eastbourne Rd', '124', '5', 'LN8 4TU', 'Cold Hanworth', 'United Kingdom', 'Kid is sleeping, please knock.'),
(8, 'Stamford Road', '67', '7', 'SP11 6ZA', 'Andover Down', 'United Kingdom', 'Buzzer is not working, please knock.'),
(9, 'Fox Lane', '31', '2', 'HR1 4BN', 'Bodenham', 'United Kingdom', 'Feel free to come into the garden, please knock on the front door.'),
(10, 'Front Street', '51', '9', 'MK44 3BF', 'Knoting', 'United Kingdom', 'Please leave the delivery at the doorstep. Paid online.'),
(11, 'Seafield Place', '100', '6B', 'GU34 7PF', 'Four Marks', 'United Kingdom', 'If I am out please deliver it to the the neighbor next door. '),
(12, 'Guild Street', '75', '2C', 'SE9 3EH', 'London', 'United Kingdom', 'Second floor, third door on the right.'),
(13, 'Boughton Rd', '14', '6A', 'WR6 2YN', 'Wichenford', 'United Kingdom', ''),
(14, 'Trehafod Road', '102', '1', 'PL26 1SU', 'Bugle', 'United Kingdom', 'Please don\'t let the cat out when you come in.'),
(15, 'Hexham Road', '38', '6F', 'DL13 9JD', 'Borthwick', 'United Kingdom', 'Don\'t forget the sides please');

-- --------------------------------------------------------

--
-- Table structure for table `chef`
--

CREATE TABLE `chef` (
  `chef_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `station` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chef`
--

INSERT INTO `chef` (`chef_id`, `user_id`, `station`) VALUES
(1, 15, 'main meal'),
(2, 14, 'dessert'),
(3, 13, 'main meal'),
(4, 12, 'dessert');

-- --------------------------------------------------------

--
-- Table structure for table `cooks`
--

CREATE TABLE `cooks` (
  `cooks_id` int(11) NOT NULL,
  `chef_id` int(11) NOT NULL,
  `meal_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cooks`
--

INSERT INTO `cooks` (`cooks_id`, `chef_id`, `meal_id`, `date`) VALUES
(1, 1, 1, '2018-07-18'),
(2, 3, 2, '2018-07-18'),
(3, 2, 3, '2018-07-18'),
(4, 4, 4, '2018-07-18'),
(5, 1, 5, '2018-07-19'),
(6, 3, 6, '2018-07-19'),
(7, 2, 7, '2018-07-19'),
(8, 4, 8, '2018-07-19'),
(9, 1, 9, '2018-07-20'),
(10, 3, 10, '2018-07-20'),
(11, 2, 11, '2018-07-20'),
(12, 4, 12, '2018-07-20'),
(13, 1, 13, '2018-07-23'),
(14, 3, 14, '2018-07-23'),
(15, 2, 15, '2018-07-23'),
(16, 4, 16, '2018-07-23');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `user_id`, `customer_status`) VALUES
(1, 1, 'valid'),
(2, 2, 'disabled'),
(3, 3, 'valid'),
(4, 4, 'inactive'),
(5, 5, 'valid'),
(6, 6, 'suspended'),
(7, 7, 'valid'),
(8, 8, 'valid'),
(9, 9, 'valid'),
(10, 10, 'inactive');

-- --------------------------------------------------------

--
-- Table structure for table `daily_menu_item`
--

CREATE TABLE `daily_menu_item` (
  `menu_item_id` int(11) NOT NULL,
  `meal_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `stock` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `daily_menu_item`
--

INSERT INTO `daily_menu_item` (`menu_item_id`, `meal_id`, `price`, `date`, `stock`) VALUES
(1, 1, '6.50', '2018-07-18', 120),
(2, 2, '4.50', '2018-07-18', 120),
(3, 3, '4.20', '2018-07-18', 120),
(4, 4, '3.95', '2018-07-18', 120),
(5, 5, '5.40', '2018-07-19', 100),
(6, 6, '6.10', '2018-07-19', 140),
(7, 7, '3.95', '2018-07-19', 110),
(8, 8, '3.95', '2018-07-19', 130),
(9, 9, '6.95', '2018-07-20', 160),
(10, 10, '5.50', '2018-07-20', 80),
(11, 11, '5.60', '2018-07-20', 90),
(12, 12, '150.00', '2018-07-20', 150),
(13, 13, '8.50', '2018-07-23', 150),
(14, 14, '5.70', '2018-07-23', 90),
(15, 15, '3.95', '2018-07-23', 75),
(16, 16, '4.95', '2018-07-23', 165);

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE `delivery` (
  `delivery_person_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `availability` tinyint(1) NOT NULL,
  `distance_from_hq` decimal(10,2) NOT NULL,
  `geo_loc_latitude` decimal(10,6) NOT NULL,
  `geo_loc_longitude` decimal(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery`
--

INSERT INTO `delivery` (`delivery_person_id`, `user_id`, `availability`, `distance_from_hq`, `geo_loc_latitude`, `geo_loc_longitude`) VALUES
(1, 11, 1, '1.20', '50.551037', '-10.708381'),
(2, 10, 1, '3.50', '52.489210', '-12.726348'),
(3, 9, 0, '0.00', '53.214099', '-5.036202'),
(4, 8, 1, '3.40', '54.441203', '4.687118');

-- --------------------------------------------------------

--
-- Table structure for table `food_order`
--

CREATE TABLE `food_order` (
  `food_order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `delivery_person_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `order_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delivery_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cost` decimal(10,2) NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eta` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `food_order`
--

INSERT INTO `food_order` (`food_order_id`, `customer_id`, `delivery_person_id`, `address_id`, `order_timestamp`, `delivery_timestamp`, `cost`, `status`, `eta`) VALUES
(1, 1, 1, 2, '2018-07-18 11:29:08', '2018-07-18 11:44:18', '12.50', 'delivered', '12:47:15'),
(2, 2, 3, 5, '2018-07-18 09:15:06', '2018-07-18 09:26:12', '9.50', 'delivered', '10:28:31'),
(3, 4, 4, 7, '2018-07-19 10:38:20', '0000-00-00 00:00:00', '11.45', 'packaging', '11:56:30'),
(4, 8, 3, 10, '2018-07-19 10:24:16', '0000-00-00 00:00:00', '15.45', 'on the way', '11:39:10');

-- --------------------------------------------------------

--
-- Table structure for table `lives`
--

CREATE TABLE `lives` (
  `lives_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lives`
--

INSERT INTO `lives` (`lives_id`, `user_id`, `address_id`) VALUES
(1, 1, 3),
(2, 8, 3),
(3, 2, 2),
(4, 3, 4),
(5, 4, 5),
(6, 5, 7),
(7, 6, 6),
(8, 7, 15),
(9, 10, 12),
(10, 9, 11),
(11, 12, 1),
(12, 11, 14),
(13, 13, 8),
(14, 14, 9),
(15, 15, 10);

-- --------------------------------------------------------

--
-- Table structure for table `meal`
--

CREATE TABLE `meal` (
  `meal_id` int(11) NOT NULL,
  `meal_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meal_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calories` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meal`
--

INSERT INTO `meal` (`meal_id`, `meal_name`, `meal_type`, `description`, `calories`) VALUES
(1, 'Venison & juniper stew', 'main', 'The Navajo love their lamb and mutton, but back in the day – at the right times of the year – they’d also get out there and hunt things like elk, which they’d stew with wild juniper berries. What’s amazing for me is that thousands of miles away in Britain we were hunting deer for venison and stewing that with juniper too. I guess some combos are just brilliant, no matter where you live. Don’t worry if you can’t get venison, because stewing beef will also be delicious. Really nice served with some rice, beans, a jacket potato or flatbreads, or, if you’re a bit more traditional, some nice steamed greens. A humble but delicious stew.', '373.00'),
(2, 'Brazilian chicken bucket', 'main', '“This Brazilian-style fried chicken is the ultimate dish to enjoy with friends and the futebol. We’ve taken the traditional Brazilian fried chicken dish frango à passarinho (which literally means ‘chicken in the style of a little bird’ because of the bite-sized pieces) and given it added crunch in the form of another well-loved Brazilian ingredient: matchstick potatoes. Best served with a beer in one hand and the remote control in the other. ”', '522.00'),
(3, 'Louis\' royal rhubarb & custard cake', 'dessert', '“Here is a beautiful celebration cake to welcome the newest addition to the royal family – HRH Louis Arthur Charles! This is a real showstopper, as you might expect – rhubarb and custard is a classic combination, and always makes me think of afternoon tea. So, put the kettle on, and dig in! ”', '509.00'),
(4, 'Pumpkin pie with pecan crumble', 'dessert', '“Topped with a sticky-sweet, nutty crumble topping, my twist on the American classic is a real treat! I’ve swapped pumpkin for butternut squash here as I love the natural sweetness it gives, plus I find it’s easier to get hold of. Enjoy! ”', '355.00'),
(5, 'Sweet potato, chickpea & spinach curry', 'main', '“This is a cheap and easy dish. You could add chopped cauliflower, red pepper or aubergine before the sweet potato; throw in shredded cooked chicken with the tomatoes; add raw prawns or white fish pieces 5 minutes before the end of the cooking time; or serve in a flatbread with grated carrot, coriander and shredded lettuce. ”', '324.00'),
(6, 'Briam', 'main', '“This popular veggie medley is traditionally eaten with feta, but it’s also awesome with pork chops. ”', '379.00'),
(7, 'Strawberry & cream sandwich sponge', 'dessert', '“This classic cake is, of course, named after Queen Victoria, the only monarch to reign longer than the current Queen. It’s relatively easy to make but does have a few quirks: you need to get as much air into it as possible, and be mindful of its sensitivity to variations in heat. Know your oven well and invest in an oven thermometer! ”', '384.00'),
(8, 'Lemon & pistachio cannoli', 'dessert', '“You’ll need either cannoli tube or cream horn moulds to make these. ”', '157.00'),
(9, 'BBQ baked beans', 'main', '“Comforting and delicious, this is a great meat-free dinner idea or, without the killer croutons, it makes a damn fine side with roasted meats. ”', '443.00'),
(10, 'Chicken & chorizo paella', 'main', '“I’ve made a few paellas in my time. The biggest one was for about 800 people in a village in Spain and it was hard work, but an incredible experience. The Spanish can be quite protective about what is and what isn’t a paella, but at the same time, the spirit of their cooking has always been flexible to whatever meat, fish, seafood or game can be found. I’ve eaten and enjoyed many paellas, and I hope you like my humble, great-value expression of one. ”', '494.00'),
(11, 'Chai spiced carrot cake', 'dessert', '“If you love carrot cake, you’ll adore this chai-spiced version, with warming tea and fragrant cinnamon, cardamom, cloves and nutmeg. ”', '708.00'),
(12, 'Vegan toffee apple upside-down cake', 'dessert', '“Sticky-sweet and topped with lightly spiced apples, this makes the perfect afternoon tea treat! ”', '340.00'),
(13, 'Hungover noodles', 'main', '“This super-tasty, quick noodle recipe is perfect when you’re feeling a little down in the dumps. ”', '483.00'),
(14, 'Classic tomato spaghetti', 'main', '“A great introduction to pasta for kids – loads of fun to eat, and a brilliant base for adding all kinds of other fresh ingredients. ”', '283.00'),
(15, 'Lamingtons', 'dessert', '“Sandwiched together with sticky-sweet raspberry jam, dunked in glossy chocolate icing, and covered with coconut, these bite-sized Aussie favourites are a real flavour sensation, and the perfect way to celebrate Australia Day! ”', '195.00'),
(16, 'Frozen cranberry cranachan', 'dessert', '“A creamy semifreddo inspired by the oaty Scottish dessert cranachan, and topped with a Drambuie cream. It ticks all the boxes when it comes to layers, flavour and texture – and better still, it can be made in advance. ”', '435.00');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `order_item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `menu_item_id` int(11) NOT NULL,
  `quantity` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`order_item_id`, `order_id`, `menu_item_id`, `quantity`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 2),
(4, 1, 3, 1),
(5, 2, 5, 2),
(6, 2, 6, 1),
(7, 3, 9, 1),
(8, 3, 12, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `email_address`, `phone_number`, `account_name`) VALUES
(1, 'John', 'Smith', 'john.smith@gmail.com', '+44 3069 990399', 'johnsm1th'),
(2, 'Bob', 'Marley', 'bob.marley12@gmail.com', '+44 3069 990203', 'bobby12m1'),
(3, 'Jane', 'Doe', 'jane122d@hotmail.com', '+44 3069 990700', 'jane122d'),
(4, 'Mary Margaret', 'McHiggins', 'marymmh@yahoo.com', '+44 3069 990626', 'mmmh12'),
(5, 'Richard Mike', 'Preston', 'richpreston11@gmail.com', '+44 3069 990431', 'richmike11'),
(6, 'Robert', 'Peterson', 'robpet11@hotmail.com', '+44 3069 990512', 'robpeterson11'),
(7, 'Jonathan', 'Reid', 'jonathan.reid1112@gmail.com', '+44 3069 990621', 'jonnyreid11'),
(8, 'Sophie Rebacca', 'McPuffins', 'sorem23@yahoo.com', '+44 3069 990023', 'sorem23'),
(9, 'Jennifer', 'Stone', 'jennifer.stone54@gmail.com', '+44 3069 990753', 'jennifers54'),
(10, 'Amanda', 'Kruger', 'amanda.kruger61@gmail.com', '+44 3069 990928', 'amandakruger61'),
(11, 'Jacob Martin', 'McKenzie', 'jacob.mckenzie33@yahoo.com', '+44 3069 990992', 'jacobmmk33'),
(12, 'Ross Robert', 'Stevenson', 'ross.stevenson41@hotmail.com', '+44 3069 990042', 'rossstevenson41'),
(13, 'Frank', 'Simestra', 'frank.simestra71@yahoo.com', '+44 3069 990337', 'frankys71'),
(14, 'Melanie', 'Cromwell', 'melanie.cromwell4@gmail.com', '+44 3069 990606', 'melaniecromwell4'),
(15, 'Craig Thomas', 'Robertson', 'craig.robertson45@yahoo.com', '+44 3069 990197', 'craigroby45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `chef`
--
ALTER TABLE `chef`
  ADD PRIMARY KEY (`chef_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cooks`
--
ALTER TABLE `cooks`
  ADD PRIMARY KEY (`cooks_id`),
  ADD KEY `chef_id` (`chef_id`,`meal_id`),
  ADD KEY `meal_id` (`meal_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `daily_menu_item`
--
ALTER TABLE `daily_menu_item`
  ADD PRIMARY KEY (`menu_item_id`),
  ADD KEY `meal_id` (`meal_id`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`delivery_person_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `food_order`
--
ALTER TABLE `food_order`
  ADD PRIMARY KEY (`food_order_id`),
  ADD KEY `customer_id` (`customer_id`,`delivery_person_id`,`address_id`),
  ADD KEY `delivery_person_id` (`delivery_person_id`),
  ADD KEY `food_order_ibfk_3` (`address_id`);

--
-- Indexes for table `lives`
--
ALTER TABLE `lives`
  ADD PRIMARY KEY (`lives_id`),
  ADD KEY `address_id` (`address_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `meal`
--
ALTER TABLE `meal`
  ADD PRIMARY KEY (`meal_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`,`menu_item_id`),
  ADD KEY `menu_item_id` (`menu_item_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `chef`
--
ALTER TABLE `chef`
  MODIFY `chef_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cooks`
--
ALTER TABLE `cooks`
  MODIFY `cooks_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `daily_menu_item`
--
ALTER TABLE `daily_menu_item`
  MODIFY `menu_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
  MODIFY `delivery_person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `food_order`
--
ALTER TABLE `food_order`
  MODIFY `food_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lives`
--
ALTER TABLE `lives`
  MODIFY `lives_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `meal`
--
ALTER TABLE `meal`
  MODIFY `meal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `order_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chef`
--
ALTER TABLE `chef`
  ADD CONSTRAINT `chef_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `cooks`
--
ALTER TABLE `cooks`
  ADD CONSTRAINT `cooks_ibfk_1` FOREIGN KEY (`chef_id`) REFERENCES `chef` (`chef_id`),
  ADD CONSTRAINT `cooks_ibfk_2` FOREIGN KEY (`meal_id`) REFERENCES `meal` (`meal_id`);

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `daily_menu_item`
--
ALTER TABLE `daily_menu_item`
  ADD CONSTRAINT `daily_menu_item_ibfk_1` FOREIGN KEY (`meal_id`) REFERENCES `meal` (`meal_id`);

--
-- Constraints for table `delivery`
--
ALTER TABLE `delivery`
  ADD CONSTRAINT `delivery_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `food_order`
--
ALTER TABLE `food_order`
  ADD CONSTRAINT `food_order_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `food_order_ibfk_2` FOREIGN KEY (`delivery_person_id`) REFERENCES `delivery` (`delivery_person_id`),
  ADD CONSTRAINT `food_order_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `address` (`address_id`);

--
-- Constraints for table `lives`
--
ALTER TABLE `lives`
  ADD CONSTRAINT `lives_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `address` (`address_id`),
  ADD CONSTRAINT `lives_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `order_item_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `food_order` (`food_order_id`),
  ADD CONSTRAINT `order_item_ibfk_2` FOREIGN KEY (`menu_item_id`) REFERENCES `daily_menu_item` (`menu_item_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
